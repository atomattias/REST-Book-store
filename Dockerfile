# runnable base
FROM ubuntu:16.04
# REPOS
RUN apt-get update
RUN apt-get install -y php apache2 libapache2-mod-php7.0 php-mysql php-intl git git-core curl php-curl php-xml composer zip unzip php-zip
## MEMCACHED
RUN apt-get install -y -q memcached
RUN pecl install memcache
## CODEIGNITER
RUN a2enmod php7 
RUN rm -rf /var/www/*

# Add sql script for the database
ADD book.sql /docker-entrypoint-initdb.d
RUN chmod -R 775 /docker-entrypoint-initdb.d
ENV MYSQL_ROOT_PASSWORD root
ADD localDump.sql /docker-entrypoint-initdb.d
#copy Application folder to  /var/www/html
RUN mkdir -P /var/www/html/
ADD book-store /var/www/html/book-store
WORKDIR /app
EXPOSE 80
## CONFIG
ENV RUNNABLE_USER_DIR /var/www/book-store
ENV RUNNABLE_SERVICE_CMDS memcached -d -u www-data; /etc/init.d/apache2 restart;
