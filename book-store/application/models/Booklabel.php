<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Booklabel extends CI_Model {

    public function __construct() {
        parent::__construct();

        //load database library
        $this->load->database();
    }

    /*
     * Fetch book_lable data
     */
    function getRows($id = ""){
        if(!empty($id)){
            $query = $this->db->get_where('book_lable', array('id' => $id));
            return $query->row_array();
        }else{
            $query = $this->db->get('book_lable');
            return $query->result_array();
        }
    }

    /*
     * Insert book_lable data
     */
    public function insert($data = array()) {

        $insert = $this->db->insert('book_lable', $data);
        if($insert){
            return 1;
        }else{
            return false;
        }
    }


}