<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Label extends CI_Model {

    public function __construct() {
        parent::__construct();

        //load database library
        $this->load->database();
    }

    /*
     * Fetch lable data
     */
    function getRows($id = ""){
        if(!empty($id)){
            $query = $this->db->get_where('label', array('id' => $id));
            return $query->row_array();
        }else{
            $query = $this->db->get('label');
            return $query->result_array();
        }
    }

    /*
     * Insert lable data
     */
    public function insert($data = array()) {

        $insert = $this->db->insert('label', $data);
        if($insert){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }


}