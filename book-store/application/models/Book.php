<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Book extends CI_Model {

    public function __construct() {
        parent::__construct();

        //load database library
        $this->load->database();
    }

    /*
     * Fetch book data
     */
    function getRows($isbn){
        if(!empty($isbn)){
            $query = $this->db->get_where('book', array('isbn' => $isbn));
            return $query->row_array();
        }else{
            $query = $this->db->get('book');
            return $query->result_array();
        }
    }


    function getRowsByTitle($title){
        if(!empty($title)){
            $query = $this->db->get_where('book', array('title' => $title));
            return $query->row_array();
        }else{
            $query = $this->db->get('book');
            return $query->result_array();
        }
    }

    /*
     * Insert book data
     */
    public function insert($data = array()) {
        if(!array_key_exists('addedon', $data)){
            $data['addedon'] = date("Y-m-d H:i:s");
        }

        $insert = $this->db->insert('book', $data);
        if($insert){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    /*
     * Update book data
     */
    public function update($data, $id) {
        if(!empty($data) && !empty($id)){
            if(!array_key_exists('addedon', $data)){
                $data['addedon'] = date("Y-m-d H:i:s");
            }
            $update = $this->db->update('book', $data, array('id'=>$id));
            return $update?true:false;
        }else{
            return false;
        }
    }

    /*
     * Delete book data
     */
    public function delete($id){
        $delete = $this->db->delete('book',array('id'=>$id));
        return $delete?true:false;
    }

}