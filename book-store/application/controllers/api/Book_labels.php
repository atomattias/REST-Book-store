<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . './libraries/REST_Controller.php';

class Book_labels extends REST_Controller
{

    public function __construct() {
        parent::__construct();

        //load book model
        $this->load->model('booklabel');
    }

    public function bookLabel_get($id = 0) {
        //returns all rows if the id parameter doesn't exist,
        //otherwise single row will be returned
        $labels = $this->booklabel->getRows($id);

        //check if the user data exists
        if(!empty($labels)){
            //set the response and exit
            $this->response($labels, REST_Controller::HTTP_OK);
        }else{
            //set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No label were found.'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function bookLabel_post() {
        $BlabelData = array();
        $BlabelData['book_id'] = $this->post('book_id');
        $BlabelData['lable_id'] = $this->post('lable_id');
        if(!empty($BlabelData['book_id']) && !empty($BlabelData['lable_id'])){
            //insert book data
            $insert = $this->booklabel->insert($BlabelData);

            //check if the book data inserted
            if($insert){
                //set the response and exit
                $this->response([
                    'status' => TRUE,
                    'message' => 'Label for a book has been added successfully.'
                ], REST_Controller::HTTP_OK);
            }else{
                //set the response and exit
                $this->response("Some problems occurred, please try again.".$insert, REST_Controller::HTTP_BAD_REQUEST);
            }
        }else{
            //set the response and exit
            $this->response("Provide complete Label information to create.", REST_Controller::HTTP_BAD_REQUEST);
        }
    }

}