<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . './libraries/REST_Controller.php';


class Example extends REST_Controller {

    public function __construct() {
        parent::__construct();

        //load book model
        $this->load->model('book');
    }

    public function book_get($isbn) {
        //returns all rows if the id parameter doesn't exist,
        //otherwise single row will be returned
        $books = $this->book->getRows($isbn);

        //check if the user data exists
        if(!empty($books)){
            //set the response and exit
            $this->response($books, REST_Controller::HTTP_OK);
        }else{
            //set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No Book were found.'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function book_post() {
        $bookData = array();
        $bookData['title'] = $this->post('title');
        $bookData['isbn'] = $this->post('isbn');
        if(!empty($bookData['title']) && !empty($bookData['isbn'])){
            //insert book data
            $insert = $this->book->insert($bookData);

            //check if the book data inserted
            if($insert){
                //set the response and exit
                $this->response([
                    'status' => TRUE,
                    'message' => 'Book has been added successfully.'
                ], REST_Controller::HTTP_OK);
            }else{
                //set the response and exit
                $this->response("Some problems occurred, please try again.", REST_Controller::HTTP_BAD_REQUEST);
            }
        }else{
            //set the response and exit
            $this->response("Provide complete book information to create.", REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function label_get() {
        $this->response("label called in Example",REST_Controller::HTTP_BAD_REQUEST);

    }

}