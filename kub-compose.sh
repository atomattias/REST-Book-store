#!/bin/bash
set fileformat=unix
curl -L http://github.com/kubernetes/kompose/releases/download/v1.12.0/kompose-linux-amd64 -o kompose
chmod +x kompose
sudo mv kompose /usr/local/bin/kompose
kompose convert
